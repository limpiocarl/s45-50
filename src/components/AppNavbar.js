import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import { Link } from "react-router-dom";
import { Fragment, useContext } from "react";
import UserContext from "../UserContext";

export default function AppNavbar() {
  const { user } = useContext(UserContext);
  // State to store user information in the login page.

  return (
    <Navbar bg="light" expand="lg">
      <Navbar.Brand as={Link} to="/" className="mx-3">
        Zuitt
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav>
          <Nav.Link as={Link} to="/" exact>
            Home
          </Nav.Link>
          <Nav.Link as={Link} to="/courses" exact>
            Courses
          </Nav.Link>
          <Nav.Link as={Link} to="/create" exact>
            Dashboard
          </Nav.Link>
          {user.id !== null ? (
            <Nav.Link as={Link} to="/logout" exact>
              Logout
            </Nav.Link>
          ) : (
            <Fragment>
              <Nav.Link as={Link} to="/register" exact>
                Register
              </Nav.Link>
              <Nav.Link as={Link} to="/login" exact>
                Login
              </Nav.Link>
            </Fragment>
          )}
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}
