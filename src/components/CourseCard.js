// import { useState } from "react";
import { Row, Col, Card } from "react-bootstrap";
import { Link } from "react-router-dom";
import "../App.css";
export default function CourseCard({ courseProp }) {
  // console.log(courseProp);
  const { name, description, price, _id, productImage1, productImage2 } =
    courseProp;

  // Activity s46
  // const [count, setCount] = useState(0);
  // const [seats, setSeats] = useState(30);
  // Syntax: const [getter, setter] = useState(initialValue)

  // function enroll() {
  //   if (seats > 0) {
  //     setCount(count + 1);
  //     setSeats(seats - 1);
  //   } else {
  //     alert("No more seats.");
  //   }
  //   // console.log("Enrollees" + count);
  // }

  return (
    <Row>
      <Col>
        <Card>
          <Card.Body>
            {/* http://localhost:4000/ - backend api server */}
            <img
              src={`http://localhost:4000/${productImage1}`}
              alt="xb"
              id="productImage"
            />
            <img
              src={`http://localhost:4000/${productImage2}`}
              alt="xb"
              id="productImage"
            />
            <Card.Title>{name}</Card.Title>
            <Card.Subtitle>Description:</Card.Subtitle>
            <Card.Text>{description}</Card.Text>
            <Card.Subtitle>Price:</Card.Subtitle>
            <Card.Text>{price}</Card.Text>
            <Link className="btn btn-primary" to={`/courses/${_id}`}>
              See More
            </Link>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}
