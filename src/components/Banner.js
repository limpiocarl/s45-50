import { Row, Col, Button } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function Banner({ data }) {
  const { header, subheader, destination, label } = data;
  return (
    <Row>
      <Col className="p-5">
        <h1>{header}</h1>
        <p>{subheader}</p>
        <Link to={destination}>
          <Button variant="primary" to={destination}>
            {label}
          </Button>
        </Link>
      </Col>
    </Row>
  );
}
