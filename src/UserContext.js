import React from "react";

// A context is a special react object which will allow us to store information within and pass it around our components within the app/
// The context object is a different approach to passing information between components without the need to pass props.
const UserContext = React.createContext();

// The "Provider"
export const UserProvider = UserContext.Provider;

export default UserContext;
