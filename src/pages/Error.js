import Banner from "../components/Banner";

export default function Error() {
  const data = {
    header: "Page Not Found",
    subheader: "Go back to homepage.",
    destination: "/",
    label: "Back to Home",
  };
  return <Banner data={data} />;
}
