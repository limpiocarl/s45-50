import { useEffect, useState, useContext } from "react";
import { Form, Button, Col } from "react-bootstrap";
import { Navigate } from "react-router-dom";
import UserContext from "../UserContext";
import Swal from "sweetalert2";

export default function Login() {
  // Allow us to consume the User Context object and its properties to use for user validation
  const { user, setUser } = useContext(UserContext);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isActive, setIsActive] = useState(false);

  function authenticate(e) {
    e.preventDefault();

    fetch("http://localhost:4000/users/login", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (typeof data.access !== "undefined") {
          localStorage.setItem("token", data.access);
          retrieveUserDetails(data.access);

          Swal.fire({
            title: "Login Successful",
            icon: "success",
            text: "Welcome to Zuitt",
          });
        } else {
          Swal.fire({
            title: "Authentication Failed",
            icon: "error",
            text: "Check your login details.",
          });
        }
      });

    // Set the email of the user in the local storage
    // Syntax: localStorage.setItem("propertyName", value)
    // localStorage.setItem("email", email);

    setEmail("");
    setPassword("");
    // alert("You are now logged in.");
  }

  const retrieveUserDetails = (token) => {
    fetch("http://localhost:4000/users/details", {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
        });
      });
  };

  useEffect(() => {
    if (email !== "" && password !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  return user.id !== null ? (
    <Navigate to="/courses" />
  ) : (
    <Col xs={12} md={6}>
      <Form onSubmit={(e) => authenticate(e)}>
        <h1>Login</h1>
        <Form.Group className="py-3">
          <Form.Label>Email Address:</Form.Label>
          <Form.Control
            type="email"
            placeholder="sample@example.com"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            required
          />
          <Form.Text className="text-muted">
            We'll never share your email to anyone.
          </Form.Text>
        </Form.Group>

        <Form.Group className="pb-3">
          <Form.Label>Password:</Form.Label>
          <Form.Control
            type="password"
            placeholder="Password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </Form.Group>

        {isActive ? (
          <Button variant="primary" type="submit" id="loginBtn">
            Login
          </Button>
        ) : (
          <Button variant="secondary" type="submit" id="loginBtn" disabled>
            Login
          </Button>
        )}
      </Form>
    </Col>
  );
}
