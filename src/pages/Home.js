import { Fragment } from "react";
import Banner from "../components/Banner";
import Highlights from "../components/Highlights";
import AddCourse from "./AddCourse";
// import CourseCard from "../components/CourseCard";

export default function Home() {
  const data = {
    header: "Zuitt Coding Bootcamp",
    subheader: "Opportunities for everyone, everywhere",
    destination: "/",
    label: "Enroll Now",
  };
  return (
    <Fragment>
      <Banner data={data} />
      <Highlights />
      <AddCourse />
    </Fragment>
  );
}
