import { useEffect, useState, useContext } from "react";
import { Form, Button, Col } from "react-bootstrap";
import UserContext from "../UserContext";
import { Navigate, useNavigate } from "react-router-dom";
import Swal from "sweetalert2";

export default function Register() {
  const { user } = useContext(UserContext);

  // State hooks to store the values of the input fields
  const [password1, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");
  const [email, setEmail] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [mobileNo, setMobileNo] = useState("");

  const history = useNavigate();

  // State to determine whether register button is enabled or not
  const [isActive, setIsActive] = useState(false);
  const [isMatch, setisMatch] = useState(true);

  async function registerUser(e) {
    // Prevents page redirection via form submission
    e.preventDefault();

    fetch("http://localhost:4000/users/checkEmail", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data === true) {
          Swal.fire({
            title: "Cannot Register",
            icon: "error",
            text: "Email is already taken by another user.",
          });
        } else {
          fetch("http://localhost:4000/users/register", {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              firstName: firstName,
              lastName: lastName,
              email: email,
              mobileNo: mobileNo,
              password: password2,
            }),
          })
            .then((res) => res.json())
            .then((data) => {
              console.log(data);
              if (data === true) {
                Swal.fire({
                  title: "You are now registered.",
                  icon: "success",
                  text: "Welcome to Zuitt",
                });
                history("/login");
              } else {
                Swal.fire({
                  title: "Registration Failed",
                  icon: "error",
                  text: "Please try again later.",
                });
              }
            });
          // This will the clear the input fields
          setFirstName("");
          setLastName("");
          setEmail("");
          setMobileNo("");
          setPassword1("");
          setPassword2("");
        }
      });
  }

  // Syntax: useEffect(() => {condition}, [parameter])

  useEffect(() => {
    if (
      firstName !== "" &&
      lastName !== "" &&
      email !== "" &&
      password1 !== "" &&
      password2 !== "" &&
      password1 === password2 &&
      mobileNo.length === 11
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [firstName, lastName, email, password1, password2, mobileNo]);

  useEffect(() => {
    if (password1 !== "" && password2 !== "" && password1 !== password2) {
      setisMatch(false);
    } else {
      setisMatch(true);
    }
  }, [password1, password2]);

  return user.id !== null ? (
    <Navigate to="/courses" />
  ) : (
    <Col xs={12} md={6}>
      <Form onSubmit={(e) => registerUser(e)}>
        <h1>Register</h1>
        <Form.Group className="py-3">
          <Form.Label>First Name:</Form.Label>
          <Form.Control
            type="text"
            value={firstName}
            onChange={(e) => setFirstName(e.target.value)}
            required
          />
        </Form.Group>
        <Form.Group className="pb-3">
          <Form.Label>Last Name:</Form.Label>
          <Form.Control
            type="text"
            value={lastName}
            onChange={(e) => setLastName(e.target.value)}
            required
          />
        </Form.Group>
        <Form.Group className="pb-3">
          <Form.Label>Email Address:</Form.Label>
          <Form.Control
            type="email"
            placeholder="sample@example.com"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            required
          />
          <Form.Text className="text-muted">
            Your email is not shared to anyone.
          </Form.Text>
        </Form.Group>
        <Form.Group className="pb-3">
          <Form.Label>Mobile Number:</Form.Label>
          <Form.Control
            type="text"
            placeholder="09XXXXXXXXX"
            value={mobileNo}
            onChange={(e) => setMobileNo(e.target.value)}
            required
          />
        </Form.Group>
        <Form.Group controlId="password1">
          <Form.Label>Password:</Form.Label>
          <Form.Control
            type="password"
            placeholder="Password"
            value={password1}
            onChange={(e) => setPassword1(e.target.value)}
            required
          />
        </Form.Group>
        <Form.Group controlId="password2 " className="pt-3">
          <Form.Label>Verify Password:</Form.Label>
          <Form.Control
            type="password"
            placeholder="Verify password"
            value={password2}
            onChange={(e) => setPassword2(e.target.value)}
            required
          />
          {isMatch ? (
            <Form.Text className="text-muted invisible">
              Password does not match.
            </Form.Text>
          ) : (
            <Form.Text className="text-muted">
              Password does not match.
            </Form.Text>
          )}
        </Form.Group>
        {isActive ? (
          <Button variant="primary" type="submit" id="submitBtn">
            Register
          </Button>
        ) : (
          <Button variant="secondary" type="submit" id="submitBtn" disabled>
            Register
          </Button>
        )}
      </Form>
    </Col>
  );
}

// Ternary Operator with the Button Tag
/* 
? - if
: -else
*/
