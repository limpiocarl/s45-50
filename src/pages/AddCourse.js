import React, { useState } from "react";
// import axios from "axios";
import { Form, Button } from "react-bootstrap";
import axios from "axios";

export default function AddCourse() {
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState("");
  const [productImage1, setProductImage1] = useState("");
  const [productImage2, setProductImage2] = useState("");

  const changeOnClick = (e) => {
    e.preventDefault();

    const formData = new FormData();
    formData.append("name", name);
    formData.append("description", description);
    formData.append("price", price);
    formData.append("productImage1", productImage1);
    formData.append("productImage2", productImage2);

    setName("");
    setDescription("");
    setPrice("");

    axios
      .post("http://localhost:4000/courses", formData)
      .then((res) => res.data)
      .then((data) => {
        if (data === true) {
          alert("Success");
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <Form onSubmit={changeOnClick} encType="multipart/form-data">
      <Form.Group className="py-3">
        <Form.Label>Name:</Form.Label>
        <Form.Control
          type="text"
          value={name}
          onChange={(e) => setName(e.target.value)}
          required
        />
      </Form.Group>
      <Form.Group className="py-3">
        <Form.Label>Description:</Form.Label>
        <Form.Control
          type="text"
          value={description}
          onChange={(e) => setDescription(e.target.value)}
          required
        />
      </Form.Group>
      <Form.Group className="py-3">
        <Form.Label>Price:</Form.Label>
        <Form.Control
          type="text"
          value={price}
          onChange={(e) => setPrice(e.target.value)}
          required
        />
      </Form.Group>
      <Form.Group>
        <Form.Label>Upload Image1:</Form.Label>
        <Form.Control
          type="file"
          name="productImage1"
          className="form-control-file"
          onChange={(e) => setProductImage1(e.target.files[0])}
        />
      </Form.Group>
      <Form.Group>
        <Form.Label>Upload Image2:</Form.Label>
        <Form.Control
          type="file"
          name="productImage2"
          className="form-control-file"
          onChange={(e) => setProductImage2(e.target.files[0])}
        />
      </Form.Group>
      <Button type="submit" className="btn btn-primary my-4">
        Add Course
      </Button>
    </Form>
  );
}
