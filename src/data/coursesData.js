const coursesData = [
  {
    id: "wd-c001",
    name: "PHP - Laravel",
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident cupiditate voluptatum iure optio.",
    price: 1000,
    onOffer: true,
  },
  {
    id: "wd-c002",
    name: "Python - Django",
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident cupiditate voluptatum iure optio.",
    price: 1000,
    onOffer: true,
  },
  {
    id: "wd-c003",
    name: "Java - Springboot",
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident cupiditate voluptatum iure optio.",
    price: 1000,
    onOffer: true,
  },
];

export default coursesData;
